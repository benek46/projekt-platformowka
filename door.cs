using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class door : MonoBehaviour
{
    private bool isOpen = false;
    public ChestWithTheKeys chestScript;

    void Start()
    {
        if(chestScript == null)
        {
            chestScript = GameObject.FindGameObjectWithTag("chest").GetComponent<ChestWithTheKeys>();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && isOpen && chestScript.ableToOpen)
        {
            OpenDoor();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isOpen = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isOpen = false;
        }
    }

    private void OpenDoor()
    {
     SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);  
    }
}
