using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;

    void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            LoadGame();
        }

        if(Input.GetKeyDown("escape") && SceneManager.GetActiveScene().buildIndex == 0)
        {
            Application.Quit();
        }

        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            LoadControl();
        }
    }

    public void LoadGame()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
        int targetLevelIndex = 2;
        StartCoroutine(LoadLevel(targetLevelIndex));
        }
    }

    public void LoadControl()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
        int targetLevelIndex = 1;
        StartCoroutine(LoadLevel(targetLevelIndex));
        }
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
        int targetLevelIndex = 0;
        StartCoroutine(LoadLevel(targetLevelIndex));
        }
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(1);

        SceneManager.LoadScene(levelIndex);
    }
}
