using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestWithTheKeys : MonoBehaviour
{
    public bool ableToOpen = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
            ableToOpen = true;
        }
    }
}